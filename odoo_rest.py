# -*- coding: utf-8 -*-
from flask import g
import gc
from common.odoo_auth import OdooServer
from common.tools import import_resources
from common.tools import auth
from common.tools import application


application.config.from_object('config.Config')

odoo_server = OdooServer(
    application.config.get('ODOO_SERVER'),
    application.config.get('ODOO_PORT'),
    application.config.get('ODOO_DB')
)


@auth.verify_password
def verify_pw(username, password):
    user = odoo_server.login(username, password)
    if user:
        g.user = user
        return True
    return False

# IMPORT RESOURCES FROM common/resources ON EXECUTION TIME
import_resources()


@application.after_request
def per_request_callbacks(response):
    gc.collect()
    return response

if __name__ == "__main__":
    application.run(port=5000, host='0.0.0.0')
