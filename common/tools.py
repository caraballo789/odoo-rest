# -*- coding: utf-8 -*-
from os.path import dirname, join, basename
from glob import glob
import re
from flask.ext.httpauth import HTTPBasicAuth
from flask import Flask
from flask_restful import Api

application = Flask(__name__)
api = Api(application)
auth = HTTPBasicAuth()


def to_camelcase(s):
    return re.sub(r'(?!^)_([a-zA-Z])',
                  lambda m: m.group(1).upper(), s.capitalize())


def import_resources():
    pwd = dirname(__file__)
    for x in glob(join(pwd, 'resources/*.py')):
        if not x.endswith('__.py'):
            filename = basename(x)[:-3]
            filename_import = 'resources.%s' % (filename,)
            odoo_resource = to_camelcase(filename)
            mod = __import__(filename_import,
                             globals(),
                             locals(),
                             fromlist=[odoo_resource])
